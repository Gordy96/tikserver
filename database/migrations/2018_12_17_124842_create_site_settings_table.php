<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('type');
            $table->string('value');
            $table->timestamps();
        });
        \Illuminate\Support\Facades\DB::table('site_settings')->insert([
            [
                'name'=>'like_price',
                'type'=>'double',
                'value'=>0
            ],[
                'name'=>'follow_price',
                'type'=>'double',
                'value'=>0
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
