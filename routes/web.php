<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['cors']], function(){
    Route::options('/{any}', function(){ return ''; })->where('any', '.*');
    Route::group(['prefix'=>'login'], function(){
        Route::get('/', 'Auth\\LoginController@showLoginForm')->name('login');
        Route::post('/', 'Auth\\LoginController@login');
    });
    Route::any('/logout', 'Auth\\LoginController@logout');
    Route::group(['prefix'=>'signup'], function(){
        Route::get('/', 'Auth\\RegisterController@showRegistrationForm');
        Route::post('/', 'Auth\\RegisterController@register');
    });
});

Route::group(['middleware'=>['domain']], function(){

    Route::group(['domain'=>'{www}api.{domain}','middleware'=>['cors']], function(){
        Route::options('/{any}', function(){ return ''; })->where('any', '.*');
        Route::group(['middleware'=>['auth:api']], function(){
            Route::group(['prefix'=>'v1'], function(){
                Route::group(['prefix'=>'orders'], function(){
                    Route::get('/', 'Api\\ApiController@OrderList');
                    Route::get('/bulk', 'Api\\ApiController@OrderListBulk');
                    Route::get('/{id}', 'Api\\ApiController@GetOrder');
                    Route::post('/', 'Api\\ApiController@MakeOrder');
                });
                Route::group(['prefix'=>'transactions'], function(){
                    Route::get('/', 'Api\\ApiController@GetTransactionsList');
                });
                Route::group(['prefix'=>'user'], function(){
                    Route::get('/', 'Api\\ApiController@GetUser');
                });
            });
        });
    });

    Route::group(['domain'=>'{www}admin.{domain}', 'middleware'=>['cors']], function(){
        Route::options('/{any}', function(){ return ''; })->where('any', '.*');
        Route::get('/', function () {
            return response()->json(\Illuminate\Support\Facades\Auth::user());
        });
        Route::group(['prefix'=>'documentation'], function(){
            Route::get('/', 'StaticController@api');
        });
        Route::group(['prefix'=>'api', 'middleware'=>['auth']], function(){
            Route::group(['prefix'=>'v1'], function(){
                Route::group(['prefix'=>'system'], function(){
                    Route::group(['prefix'=>'settings'], function(){
                        Route::get('/', 'Admin\\Api\\SystemController@GetSettings');
                        Route::post('/', 'Admin\\Api\\SystemController@SetSettings');
                    });
                });
                Route::group(['prefix'=>'users'], function(){
                    Route::get('/', 'Admin\\Api\\UserController@UserList');
                    Route::group(['prefix'=>'/{uid}'], function(){
                        Route::get('/', 'Admin\\Api\\UserController@GetUser');
                        Route::group(['prefix'=>'orders'], function(){
                            Route::get('/', 'Admin\\Api\\UserController@GetUserOrders');
                            Route::get('/{id}', 'Admin\\Api\\UserController@GetUserOrder');
                        });
                        Route::group(['prefix'=>'transactions'], function(){
                            Route::get('/', 'Admin\\Api\\UserController@GetTransactionsList');
                        });
                        Route::group(['prefix'=>'balance'], function(){
                            Route::any('/', 'Admin\\Api\\UserController@SetUserBalance');
                        });
                        Route::group(['prefix'=>'roles'], function(){
                            Route::any('/attach', 'Admin\\Api\\UserController@GiveRole');
                            Route::any('/detach', 'Admin\\Api\\UserController@RemoveRole');
                        });
                    });
                });
                Route::group(['prefix'=>'orders'], function(){
                    Route::get('/', 'Admin\\Api\\OrderController@OrderList');
                    Route::get('/{id}', 'Admin\\Api\\OrderController@GetOrder');
                });
            });
        });
    });

    Route::any(config('l5-swagger.routes.docs').'/{jsonFile?}', [
        'as' => 'l5-swagger.docs',
        'middleware' => config('l5-swagger.routes.middleware.docs', []),
        'uses' => '\L5Swagger\Http\Controllers\SwaggerController@docs',
    ]);

    Route::get(config('l5-swagger.routes.docs').'/asset/{asset}', [
        'as' => 'l5-swagger.asset',
        'middleware' => config('l5-swagger.routes.middleware.asset', []),
        'uses' => '\L5Swagger\Http\Controllers\SwaggerAssetController@index',
    ]);

    Route::get(config('l5-swagger.routes.oauth2_callback'), [
        'as' => 'l5-swagger.oauth2_callback',
        'middleware' => config('l5-swagger.routes.middleware.oauth2_callback', []),
        'uses' => '\L5Swagger\Http\Controllers\SwaggerController@oauth2Callback',
    ]);

    Route::group(['domain'=>'{www}{domain}', 'middleware'=>['cors']],function(){

        Route::group(['prefix'=>'documentation'], function(){
            Route::get('/', 'StaticController@api');
        });

        Route::options('/{any}', function(){ return ''; })->where('any', '.*');
        Route::group(['prefix'=>'api', 'middleware'=>['auth']], function(){
            Route::group(['prefix'=>'v1'], function(){
                Route::group(['prefix'=>'order'], function(){
                    Route::any('/', 'Api\\OrderController@MakeOrder');
                });
                Route::group(['prefix'=>'user'], function(){
                    Route::get('/', 'Api\\UserController@GetUser');
                    Route::group(['prefix'=>'orders'], function(){
                        Route::get('/', 'Api\\UserController@OrderList');
                        Route::get('/{id}', 'Api\\UserController@GetOrder');
                    });
                    Route::group(['prefix'=>'transactions'], function(){
                        Route::get('/', 'Api\\UserController@GetTransactionsList');
                    });
                });
            });
        });

        Route::get('/', function () {
            return response()->json(\Illuminate\Support\Facades\Auth::user());
        });
    });
});

