<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderType extends Model
{
    use Stringable;

    protected $fillable = [
        'name'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->field_name = "name";
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'status_id');
    }

    public function getPrice($quantity)
    {
        return $quantity;
    }
}
