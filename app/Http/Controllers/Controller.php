<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function SuccessfulJsonResponse($body, $status = 200)
    {
        return response()->json([
            'success'=>true,
            'result'=>$body
        ], $status);
    }
    public function FailedJsonResponse($body, $status = 400)
    {
        return response()->json([
            'success'=>false,
            'error'=>$body
        ], $status);
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null|\App\User
     */
    public function user()
    {
        return Auth::user();
    }
}
