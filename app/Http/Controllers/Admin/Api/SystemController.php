<?php

namespace App\Http\Controllers\Admin\Api;

use App\SiteSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SystemController extends Controller
{
    public function GetSettings(Request $request)
    {
        $config = SiteSetting::all()->mapWithKeys(function($e){
            return [$e->name => ['value' => $e->value, 'type' => $e->type]];
        });
        return $this->SuccessfulJsonResponse($config);
    }

    public function SetSettings(Request $request)
    {
        $rules = [];
        foreach($request->keys() as $key){
            $rules[$key] = [
                'required',
                function ($attribute, $value, $fail) {
                    if (SiteSetting::query()->where('name', $attribute)->first()) {
                        return;
                    }
                    $fail($attribute.' is invalid.');
                }
            ];
        }
        $filtered = $this->validate($request, $rules);
        $settings = SiteSetting::query()->whereIn('name', array_keys($filtered))->get();
        foreach($settings as $setting){
            $setting->value = $filtered[$setting->name];
            $setting->save();
        }
        return $this->GetSettings($request);
    }
}
