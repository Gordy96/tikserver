<?php

namespace App\Http\Controllers\Admin\Api;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function OrderList(Request $request)
    {
        $pagination = Order::with(['type', 'transaction', 'user', 'status'])->paginate();
        return $this->SuccessfulJsonResponse($pagination);
    }

    public function GetOrder(Request $request, $id)
    {
        $order = Order::with(['type', 'transaction', 'user', 'status'])->find($id);
        return $this->SuccessfulJsonResponse($order);
    }
}
