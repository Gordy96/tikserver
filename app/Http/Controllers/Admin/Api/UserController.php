<?php

namespace App\Http\Controllers\Admin\Api;

use App\Role;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function UserList(Request $request)
    {
        $query = User::with(['balance','roles']);

        if($request->has('email'))
            $query = $query->where('email', 'like', "%{$request->get('email')}%");
        if($request->has('name'))
            $query = $query->where('name', 'like', "%{$request->get('name')}%");

        return $this->SuccessfulJsonResponse($query->paginate());
    }

    public function GetUser(Request $request, $id)
    {
        $user = User::with(['balance', 'roles'])->findOrFail($id);
        return $this->SuccessfulJsonResponse($user);
    }

    public function GetUserOrders(Request $request, $id)
    {
        $user = User::findOrFail($id);
        return $this->SuccessfulJsonResponse($user->orders()->with(['transaction','type','status'])->paginate());
    }

    public function GetUserOrder(Request $request, $uid, $oid)
    {
        $user = User::findOrFail($uid);
        return $this->SuccessfulJsonResponse($user->orders()->with(['transaction','type','status'])->find($oid));
    }

    public function GetTransactionsList(Request $request, $uid)
    {
        $user = User::findOrFail($uid);
        return $this->SuccessfulJsonResponse($user->transactions()->with(['status'])->paginate());
    }

    public function SetUserBalance(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'value'=>['required']
            ]);
        } catch (ValidationException $e){
            return $this->FailedJsonResponse($e);
        }
        $user = User::with([])->findOrFail($id);
        $value = $request->get('value');
        $transaction = $user->balance->transactions()->save(new Transaction([
                'debit'=>$value < 0,
                'value'=>abs($value),
                'status_id'=>2
        ]));
        $user->balance->calculate();
        return $this->SuccessfulJsonResponse($transaction);
    }

    public function GiveRole(Request $request, $uid)
    {
        $this->validate($request, [
            'role'=>['required']
        ]);
        $user = User::findOrFail($uid);
        try {
            $user->roles()->attach(Role::where("name", $request->get('role'))->firstOrFail());
        } catch (\Throwable $e){
            return $this->FailedJsonResponse("wrong parameter");
        }
        return $this->SuccessfulJsonResponse($user->refresh()->load(['roles','balance']));
    }

    public function RemoveRole(Request $request, $uid)
    {
        $this->validate($request, [
            'role'=>['required']
        ]);
        $user = User::findOrFail($uid);
        $user->roles()->detach(Role::where("name", $request->get('role'))->firstOrFail());
        return $this->SuccessfulJsonResponse($user->refresh()->load(['roles','balance']));
    }
}
