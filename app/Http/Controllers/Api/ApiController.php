<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Order;
use App\Exceptions\InsufficientFundsException;
use App\Transaction;
use App\OrderType;

class ApiController extends Controller
{

    public function MakeOrder(Request $request)
    {
        $this->validate($request, [
            'type'=>['required', 'exists:order_types,name'],
            'quantity'=>['required','integer'],
            'username'=>['required']
        ]);

        $type = OrderType::where('name', $request->get('type'))->firstOrFail();
        $quantity = $request->get('quantity');

        $price = $type->getPrice($quantity);
        $user = $this->user();

        if($user->balance->value < $price)
            throw new InsufficientFundsException();

        $transaction = $user->balance->transactions()->save(new Transaction([
            'debit'=>true,
            'value'=>$price,
            'status_id'=>2
        ]));
        $order = $user->orders()->save(new Order([
            'username'=>$request->get('username'),
            'quantity'=>$quantity,
            'status_id'=>1,
            'type_id'=>$type->id,
            'transaction_id'=>$transaction->id,
        ]));
        $user->balance->calculate();
        return $this->SuccessfulJsonResponse($order->load(['transaction','type','status']));
    }

    public function OrderListBulk(Request $request)
    {
        $this->validate($request, [
            'ids'=>['required']
        ]);

        $ids = explode(',', $request->get('ids'));
        $pagination = $this->user()->orders()->with(['type', 'transaction','status'])->whereIn('id', $ids)->get();
        return $this->SuccessfulJsonResponse($pagination);
    }

    public function OrderList(Request $request)
    {
        $pagination = $this->user()->orders()->with(['type', 'transaction','status'])->paginate();
        return $this->SuccessfulJsonResponse($pagination);
    }

    public function GetOrder(Request $request, $id)
    {
        $order = $this->user()->orders()->with(['type', 'transaction','status'])->find($id);
        return $this->SuccessfulJsonResponse($order);
    }

    public function GetTransactionsList(Request $request)
    {
        return $this->SuccessfulJsonResponse($this->user()->transactions()->with(['status'])->paginate());
    }

    public function GetUser(Request $request)
    {
        return $this->SuccessfulJsonResponse($this->user()->load(['balance'])->makeHidden(['created_at','updated_at', 'email_verified_at']));
    }
}
