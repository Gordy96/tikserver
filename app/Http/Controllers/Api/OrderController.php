<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InsufficientFundsException;
use App\Order;
use App\OrderStatus;
use App\OrderType;
use App\Transaction;
use App\TransactionStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function __construct()
    {
        OrderStatus::$mode = true;
        OrderType::$mode = true;
        TransactionStatus::$mode = true;
    }

    public function MakeOrder(Request $request)
    {
        $this->validate($request, [
            'type'=>['required', 'exists:order_types,name'],
            'quantity'=>['required','integer'],
            'username'=>['required']
        ]);

        $type = OrderType::where('name', $request->get('type'))->firstOrFail();
        $quantity = $request->get('quantity');

        $price = $type->getPrice($quantity);
        $user = $this->user();

        if($user->balance->value < $price)
            throw new InsufficientFundsException();

        $transaction = $user->balance->transactions()->save(new Transaction([
            'debit'=>true,
            'value'=>$price,
            'status_id'=>2
        ]));
        $order = $user->orders()->save(new Order([
            'username'=>$request->get('username'),
            'quantity'=>$quantity,
            'status_id'=>1,
            'type_id'=>$type->id,
            'transaction_id'=>$transaction->id,
        ]));
        $user->balance->calculate();
        return $this->SuccessfulJsonResponse($order->load(['transaction','type','status']));
    }
}
