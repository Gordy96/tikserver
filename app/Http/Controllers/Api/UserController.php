<?php

namespace App\Http\Controllers\Api;

use App\OrderStatus;
use App\OrderType;
use App\TransactionStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct()
    {
        OrderStatus::$mode = true;
        OrderType::$mode = true;
        TransactionStatus::$mode = true;
    }
    public function OrderList(Request $request)
    {
        $pagination = $this->user()->orders()->with(['type', 'transaction','status'])->paginate();
        return $this->SuccessfulJsonResponse($pagination);
    }

    public function GetOrder(Request $request, $id)
    {
        $order = $this->user()->orders()->with(['type', 'transaction','status'])->find($id);
        return $this->SuccessfulJsonResponse($order);
    }

    public function GetUser(Request $request)
    {
        return $this->SuccessfulJsonResponse($this->user()->load(['balance', 'roles']));
    }

    public function GetTransactionsList(Request $request)
    {
        return $this->SuccessfulJsonResponse($this->user()->transactions()->with(['status'])->paginate());
    }
}
