<?php

namespace App\Http\Middleware;

use Closure;

class SubDomainHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $route->forgetParameter('domain');
        $route->forgetParameter('www');
        return $next($request);
    }
}
