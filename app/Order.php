<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'username',
        'quantity',
        'status_id',
        'type_id',
        'transaction_id',
        'user_id'
    ];

    protected $hidden = [
        'type_id',
        'status_id',
        'transaction_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id');
    }

    public function type()
    {
        return $this->belongsTo(OrderType::class, 'type_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }
}
