<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class SiteSetting extends Model
{
    protected $fillable = [
        'name',
        'type',
        'value'
    ];

    private static function castValueToType($value, $type)
    {
        if (is_null($value)) {
            return $value;
        }

        $ins = new static();

        switch ($type) {
            case 'int':
            case 'integer':
                return (int) $value;
            case 'real':
            case 'float':
            case 'double':
                return $ins->fromFloat($value);
            case 'string':
                return (string) $value;
            case 'bool':
            case 'boolean':
                return (bool) $value;
            case 'object':
                return $ins->fromJson($value, true);
            case 'array':
            case 'json':
                return $ins->fromJson($value);
            case 'collection':
                return new Collection($ins->fromJson($value));
            case 'date':
                return $ins->asDate($value);
            case 'datetime':
            case 'custom_datetime':
                return $ins->asDateTime($value);
            case 'timestamp':
                return $ins->asTimestamp($value);
            default:
                return $value;
        }
    }

    public function getValueAttribute($value)
    {
        return self::castValueToType($value, $this->type);
    }

    public function setValueAttribute($value)
    {
        $v = self::castValueToType($value, $this->type);
        $this->attributes['value'] = $v;
    }
}
