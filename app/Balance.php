<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = [
        'value',
        'user_id'
    ];

    protected $hidden = [
        'id',
        'user_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'balance_id');
    }

    public function calculate(){
        $this->value = $this->transactions()->where('debit', false)->sum('value') - $this->transactions()->where('debit', true)->sum('value');
        $this->save();
    }
}
