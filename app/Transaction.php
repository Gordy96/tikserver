<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'debit',
        'value',
        'balance_id',
        'status_id'
    ];

    protected $hidden = [
        'user_id',
        'balance_id',
        'status_id'
    ];

    public $casts = [
        'debit'=>'boolean',
        'value'=>'double'
    ];

    public function status()
    {
        return $this->belongsTo(TransactionStatus::class, 'status_id');
    }

    public function balance()
    {
        return $this->belongsTo(Balance::class, 'balance_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'transaction_id');
    }
}
