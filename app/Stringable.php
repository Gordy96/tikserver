<?php

namespace App;

trait Stringable {

    public static $mode = false;
    protected $field_name;

    public function toArray()
    {
        return static::$mode == true ? $this->{$this->field_name} : parent::toArray();
    }
}