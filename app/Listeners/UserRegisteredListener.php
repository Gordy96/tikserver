<?php

namespace App\Listeners;

use App\Balance;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $event->user->balance()->save(new Balance([
            'value'=>0
        ]));
        $event->user->api_key = str_random(128);
        $event->user->save();
        $event->user->roles()->attach(1);
    }
}
