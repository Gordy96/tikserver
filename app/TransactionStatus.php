<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{

    use Stringable;

    protected $fillable = [
        'name'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->field_name = "name";
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'status_id');
    }
}
